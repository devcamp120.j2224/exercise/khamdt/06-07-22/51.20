package com.devcamp.j51;

import java.text.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


public class J5110 {
    public static void main(String[] args) {
        /*
        LocalDateTime myDate = LocalDateTime.now();
        System.out.println("Before format is " + myDate);
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:SS");
        String date = formatter.format(myDate);
        System.out.println("After format is " + date);
        J5110 j5110 = new J5110();
        System.out.println("After format Date " +  j5110.niceDate());
        System.out.println("After format Date localization " +  j5110.getVietNamDate());
        */
        
        Customer customer = new Customer();
        System.out.println("my format is "+ customer.myName);
        System.out.println("my customer " + customer.myName);
        System.out.println("--------------------------------");
        Customer customer1 = new Customer(2, 0.3f, 'C', false, "Hieu");
        System.out.println("my format is "+ customer1.myName);
        System.out.println("my customer " + customer1.myName);
        System.out.println("my customer " + customer1.myCharacter);

        WrapperCustomer customer2 = new WrapperCustomer();
        System.out.println("my format is "+ customer2.myName);
        System.out.println("my customer " + customer2.myName);
        System.out.println("--------------------------------");
        WrapperCustomer customer3 = new WrapperCustomer(2, 0.3f, 'C', false, "Hieu");
        System.out.println("my format is "+ customer3.myName);
        System.out.println("my customer " + customer3.myName);
        System.out.println("my customer " + customer3.myCharacter);
    }
    public String niceDate() { 
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        java.util.Date myDate = new java.util.Date();
        return String.format("It is new date "+dateFormat.format(myDate));
        
    }
    public String getVietNamDate() { 
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, dd-MM-yyyy")
        .localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return String.format("It is new date "+formatter.format(today));
    }
}
