package com.devcamp.j51;

public class Customer {
    int myNumber;
    float myFloatNumber;
    char myCharacter;
    boolean myBoolean;
    String myName;
    public Customer() { 
        myCharacter='H';
        this.myBoolean = true;
        this.myName = "Hieu";

    }
    public Customer(int myNumber, float myFloatNumber, char myCharacter, boolean myBoolean, String myName) {
        this.myNumber = myNumber;
        this.myFloatNumber = myFloatNumber;
        this.myCharacter = myCharacter;
        this.myBoolean = myBoolean;
        this.myName = myName;
    }
}
