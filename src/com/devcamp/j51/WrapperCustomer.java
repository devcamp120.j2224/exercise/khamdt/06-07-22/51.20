package com.devcamp.j51;

public class WrapperCustomer {
    Integer myNumber = 2;
    Float myFloatNumber;
    Character myCharacter;
    Boolean myBoolean = true;
    String myName;
    public WrapperCustomer() { 
        myCharacter='H';
        this.myBoolean = true;
        this.myName = "Hieu";

    }
    public WrapperCustomer(int myNumber, float myFloatNumber, char myCharacter, boolean myBoolean, String myName) {
        this.myNumber = myNumber;
        this.myFloatNumber = myFloatNumber;
        this.myCharacter = myCharacter;
        this.myBoolean = myBoolean;
        this.myName = myName;
    }
}
